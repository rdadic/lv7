﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp22
{
    public partial class Form1 : Form
    {
        int turn = 1;
        string prvi, drugi;
        Graphics g;
        int A1=0, A2=0, A3=0, A4=0, A5=0, A6=0, A7=0, A8=0, A9=0;
        List<Pen> pens = new List<Pen>();
        
        int count = 0;

        public Form1()
        {
            InitializeComponent();
            g = groupBox1.CreateGraphics();
            pens.Add(new Pen(Color.Aquamarine, 1.5F));
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            pictureBox1.Image = null;
            pictureBox2.Image = null;
            pictureBox3.Image = null;
            pictureBox4.Image = null;
            pictureBox5.Image = null;
            pictureBox6.Image = null;
            pictureBox7.Image = null;
            pictureBox8.Image = null;
            pictureBox9.Image = null;
            textBox1.Clear();
            textBox2.Clear();
            count = 0;
            turn = 1;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {
            foreach(PictureBox p in groupBox1.Controls)
            {
                
                {

                }
                
            }
        }
         
private void textBox3_TextChanged(object sender, EventArgs e)
        {
           
        }
        interface IDrawable
        {
            void draw(int X, int Y, Pen p, Graphics g);

        }
        class Circle : IDrawable
        {
            int r;
            public Circle() { r = 25; }
            public void draw(int x, int y, Pen p, Graphics g)
            {
                g.DrawEllipse(p, x, y, r, r);
            }


        }
        class Iks: IDrawable
        {
            public void draw (int X, int Y, Pen p, Graphics g)
            {
                String drawString = "X";
                Font drawFont = new Font("Arial", 35);
                SolidBrush drawBrush = new SolidBrush(Color.Aquamarine);
               float X1 = Convert.ToSingle(X);
                float Y1 = Convert.ToSingle(Y);
                g.DrawString(drawString, drawFont, drawBrush, X1,Y1);


            }
        }

        private void mouse1(object sender, MouseEventArgs e)
        {
            if (turn == 1)
            {
                g = pictureBox1.CreateGraphics();
                Iks x = new Iks();
                x.draw(e.X, e.Y, pens[0], g);
                A1 = 1;
                turn = 2;
                Kraj();

                count++;
            }
            else
            {
                g = pictureBox1.CreateGraphics();
                Circle c = new Circle();
                c.draw(e.X, e.Y, pens[0], g);
                A1 = 2;
                turn = 1;
                Kraj();
                count++;
            }
        }

        private void mouse2(object sender, MouseEventArgs e)
        {
            
            if (turn == 1)
            {
                g = pictureBox2.CreateGraphics();
                Iks x = new Iks();
                x.draw(e.X, e.Y, pens[0], g);
                A2 = 1;
                turn = 2;
                Kraj();
                Igrac();
                count++;
            }
            else
            {
                g = pictureBox2.CreateGraphics();
                Circle c = new Circle();
                c.draw(e.X, e.Y, pens[0], g);
                A2 = 2;
                turn = 1;
                Kraj();
                Igrac();
                count++;
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            drugi = textBox2.Text;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            prvi = textBox1.Text;
            if (turn == 1)
            {
                textBox3.Text = prvi;
            }
        }

        private void mouse3(object sender, MouseEventArgs e)
        {
           
            if (turn == 1)
            {
                g = pictureBox3.CreateGraphics();
                Iks x = new Iks();
                x.draw(e.X, e.Y, pens[0], g);
                A3 = 1;
                turn = 2;
                Kraj();
                Igrac();
                count++;
            }
            else
            {
                g = pictureBox3.CreateGraphics();
                Circle c = new Circle();
                c.draw(e.X, e.Y, pens[0], g);
                A3 = 2;
                turn = 1;
                Kraj();
                Igrac();
                count++;
            }
        }

        private void mouse4(object sender, MouseEventArgs e)
        {
            if (turn == 1)
            {
                g = pictureBox4.CreateGraphics();
                Iks x = new Iks();
                x.draw(e.X, e.Y, pens[0], g);
                A4 = 1;
                turn = 2;
                Kraj();
                Igrac();
                count++;
            }
            else
            {
                g = pictureBox4.CreateGraphics();
                Circle c = new Circle();
                c.draw(e.X, e.Y, pens[0], g);
                A4 = 2;
                turn = 1;
                Kraj();
                Igrac();
                count++;
            }
        }

        private void mouse5(object sender, MouseEventArgs e)
        {
            if (turn == 1)
            {
                g = pictureBox5.CreateGraphics();
                Iks x = new Iks();
                x.draw(e.X, e.Y, pens[0], g);
                A5 = 1;
                turn = 2;
                Kraj();
                Igrac();
                count++;
            }
            else
            {
                g = pictureBox5.CreateGraphics();
                Circle c = new Circle();
                c.draw(e.X, e.Y, pens[0], g);
                A5 = 2;
                turn = 1;
                Kraj();
                Igrac();
                count++;
            }
        }

        private void mouse6(object sender, MouseEventArgs e)
        {
            if (turn == 1)
            {
                g = pictureBox6.CreateGraphics();
                Iks x = new Iks();
                x.draw(e.X, e.Y, pens[0], g);
                A6 = 1;
                turn = 2;
                Kraj();
                Igrac();
                count++;
            }
            else
            {
                g = pictureBox6.CreateGraphics();
                Circle c = new Circle();
                c.draw(e.X, e.Y, pens[0], g);
                A6 = 2;
                turn = 1;
                Kraj();
                Igrac();
                count++;
            }
        }

        private void mouse7(object sender, MouseEventArgs e)
        {
            if (turn == 1)
            {
                g = pictureBox7.CreateGraphics();
                Iks x = new Iks();
                x.draw(e.X, e.Y, pens[0], g);
                A7 = 1;
                turn = 2;
                Kraj();
                Igrac();
                count++;
            }
            else
            {
                g = pictureBox7.CreateGraphics();
                Circle c = new Circle();
                c.draw(e.X, e.Y, pens[0], g);
                turn = 1;
                A7 = 2;
                Kraj();
                Igrac();
                count++;
            }
        }

        private void mouse8(object sender, MouseEventArgs e)
        {
            if (turn == 1)
            {
                g = pictureBox8.CreateGraphics();
                Iks x = new Iks();
                x.draw(e.X, e.Y, pens[0], g);
                A8= 1;
                turn = 2;
                Kraj();
               
                Igrac();
                count++;
            }
            else
            {
                g = pictureBox8.CreateGraphics();
                Circle c = new Circle();
                c.draw(e.X, e.Y, pens[0], g);
                turn = 1;
                A8 = 2;
                Kraj();
                Igrac();
                count++;
            }
        }

        private void mouse9(object sender, MouseEventArgs e)
        {
            if (turn == 1)
            {
                g = pictureBox9.CreateGraphics();
                Iks x = new Iks();
                x.draw(e.X, e.Y, pens[0], g);
                A9= 1;
                turn = 2;
                Kraj();
                Igrac();
                count++;
            }
            else
            {
                g = pictureBox9.CreateGraphics();
                Circle c = new Circle();
                c.draw(e.X, e.Y, pens[0], g);
                turn = 1;
                A9 = 2;
                Kraj();
                Igrac();
                count++;
            }
        }
        public void Kraj()
        {
            if (count >= 9)
            {
                if((A1==1 && A2==1 && A3==1) ||(A4==1 && A5==1 && A6==1) ||(A7==1 && A8==1 && A9==1))
               
                {
                    MessageBox.Show("Prvi je pobijedio");

                }
                if ((A1== 2 && A2 == 2 && A3 == 2) || (A4 == 2 && A5 == 2 && A6 == 2) || (A7 == 2 && A8 == 2 && A9 == 2))
                {
                    MessageBox.Show("Drugi je pobijedio");
                    
                }
                if((A1==1 && A4==1 && A7==1)||(A2==1 && A5==1 && A8==1)||(A3==1 && A6==1 && A9 == 1))
                {
                    MessageBox.Show("Prvi je pobijedio");
                }
                if ((A1 == 2 && A4 == 2 && A7 == 2) || (A2 == 2 && A5 == 2 && A8 == 2) || (A3 == 2 && A6 == 2 && A9 == 2))
                {
                    MessageBox.Show("Drugi je pobijedio");

                }
                if((A1 == 1 && A5 == 1 && A9 == 1) || (A3==1 && A5==1 && A7 == 1))
                {
                    MessageBox.Show("Prvi je pobijedio");

                }
                if ((A1 == 2 && A5 == 2 && A9 == 2) || (A3 == 2 && A5 == 2 && A7 == 2))
                {
                    MessageBox.Show("Drugi je pobijedio");
                }

            }
            if (count >= 3)
            {
                if ((A1 == 1 && A2 == 1 && A3 == 1) || (A4 == 1 && A5 == 1 && A6 == 1) || (A7 == 1 && A8 == 1 && A9 == 1))

                {
                    MessageBox.Show("Prvi je pobijedio");

                }
                if ((A1 == 2 && A2 == 2 && A3 == 2) || (A4 == 2 && A5 == 2 && A6 == 2) || (A7 == 2 && A8 == 2 && A9 == 2))
                {
                    MessageBox.Show("Drugi je pobijedio");

                }
                if ((A1 == 1 && A4 == 1 && A7 == 1) || (A2 == 1 && A5 == 1 && A8 == 1) || (A3 == 1 && A6 == 1 && A9 == 1))
                {
                    MessageBox.Show("Prvi je pobijedio");
                }
                if ((A1 == 2 && A4 == 2 && A7 == 2) || (A2 == 2 && A5 == 2 && A8 == 2) || (A3 == 2 && A6 == 2 && A9 == 2))
                {
                    MessageBox.Show("Drugi je pobijedio");

                }
                if ((A1 == 1 && A5 == 1 && A9 == 1) || (A3 == 1 && A5 == 1 && A7 == 1))
                {
                    MessageBox.Show("Prvi je pobijedio");

                }
                if ((A1 == 2 && A5 == 2 && A9 == 2) || (A3 == 2 && A5 == 2 && A7 == 2))
                {
                    MessageBox.Show("Drugi je pobijedio");
                }
            }

        }
        public void Igrac()
        {
            if (turn == 1)
            {
                textBox3.Text = prvi;
            }
            else
            {
                textBox3.Text = drugi;
            }
        }
       

    }
}
